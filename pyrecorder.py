
# libraries for video recording 
import numpy as np
import cv2 as cv

# libraries for arduino interfacing. 
import serial
import re
import time

# define general variables. Future should make these are arguements in function call.
trigger_target = 3 # number of trigger after which to start recordings. Anything between 3 to 5. 
video_duration = 310 # video duration in seconds

# for saving the file.
subID = '408718'  #ID of the subject
sesID = 'testcam' #session number
runID = '3' #run number


# define the path to the arduino
ser = serial.Serial('/dev/ttyACM0', 9600, timeout=1)

# trigger detection module
trigger_count = 0
while True:
	line =  ser.readline()
	string = line.decode()
	if re.match('T', string):
		trigger_count = trigger_count +1
		if trigger_count == trigger_target:
			break
		print('Trigger')
		time.sleep(0.5)
		ser.flushInput()
		
		
# video recording module
		
cap = cv.VideoCapture(4)  #should be 4 for external, 1 for webcam  

# Define the codec and create VideoWriter object. XVID seems to work. Make sure to install libraries 
fourcc = cv.VideoWriter_fourcc(*'XVID')
#fourcc = cv.VideoWriter_fourcc('M','J','P','G')

# Define the resolution. check with your camera. 
#out = cv.VideoWriter('sub-'+subID+'_ses-'+sesID+'_run-'+runID+'.avi', fourcc, 29.970, (720,  480))
out = cv.VideoWriter('sub-'+subID+'_ses-'+sesID+'_run-'+runID+'.avi', fourcc, 25, (640,  480))
#out = cv.VideoWriter('sub-'+subID+'_ses-'+sesID+'_run-'+runID+'.avi', fourcc, 11, (640,  480))


start = time.time()
while cap.isOpened():
    ret, frame = cap.read()
    if not ret:
        print("Can't receive frame (stream end?). Exiting ...")
        break
    frame = cv.flip(frame, 0)
    # write the flipped frame
    out.write(frame)
    cv.imshow('frame', frame)
    if cv.waitKey(1) == ord('q') or time.time() - start > video_duration:
        break
# Release everything if job is finished
cap.release()
out.release()
cv.destroyAllWindows()
