import pyfirmata, pyfirmata.util
import time

board = pyfirmata.Arduino('/dev/ttyACM0')

iterator = pyfirmata.util.Iterator(board)
iterator.start()


button = board.get_pin('d:10:i')

#board.digital[10].write(1)
button.enable_reporting()
# delay for a second
time.sleep(1)

for i in range(100):
    # The return values are: True False, and None
    if str(button.read()) == 'True':
        print("Button pressed")
    elif str(button.read()) == 'False':
        print("Button not pressed")
    else: 
        print("Button was never pressed")
    board.pass_time(0.5)


board.exit()



